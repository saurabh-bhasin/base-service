﻿namespace SorterService.DataServices.Models
{
    public enum JobStatus
    {
        PENDING,
        COMPLETED,
        FAILED
    }
}
