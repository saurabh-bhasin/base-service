﻿using System;
using System.Collections.Generic;

namespace SorterService.DataServices.Models
{
    public class JobResultModel
    {
        public Guid Identifier { get; set; }
        public DateTime CreationDateTime { get; set; }
        public double DurationInSeconds { get; set; }
        public string JobStatus { get; set; }
        public List<int> Input { get; set; }
        public List<int> Output { get; set; }
    }
}