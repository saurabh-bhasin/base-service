﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using SorterService.Controllers;
using SorterService.DataServices.Interfaces;
using SorterService.DataServices.Models;
using System;
using System.Collections.Generic;

namespace SorterService.UnitTests.Controller
{
    [TestFixture, Category("Controller")]
    public class SortingJobControllerTests
    {
        private Mock<ILogger<SortingJobController>> _logger;
        private Mock<IJobDataService> _jobDataService;
        private SortingJobController _sortingJobController;

        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger<SortingJobController>>();
            _jobDataService = new Mock<IJobDataService>();
            _sortingJobController = new SortingJobController(_logger.Object, _jobDataService.Object);
        }

        #region v1.0/sorting-job/Get/Id

        [Test]
        public void GetById_ShouldReturnNotFoundIfNoRecordExists()
        {
            Guid identifier = Guid.NewGuid();
            string message = "Test message";
            _jobDataService.Setup(x => x.Exists(identifier, out message)).Returns(false);

            var result = _sortingJobController.Get(identifier);
            var notFoundResult = result as NotFoundObjectResult;

            // Returns NotFound
            Assert.IsNotNull(notFoundResult);
            Assert.AreEqual(404, notFoundResult.StatusCode);
            Assert.AreEqual(message, notFoundResult.Value);
        }

        [Test]
        public void GetById_ShouldReturnSuccessIfRecordExists()
        {
            Guid identifier = Guid.NewGuid();
            var jobResultModel = new JobResultModel
            {
                Identifier = identifier,
                Input = new List<int> { 2, 1 },
                CreationDateTime = DateTime.UtcNow,
                JobStatus = JobStatus.PENDING.ToString()
            };

            string message = string.Empty;
            _jobDataService.Setup(x => x.Exists(identifier, out message)).Returns(true);
            _jobDataService.Setup(x => x.Get(identifier)).Returns(jobResultModel);

            var result = _sortingJobController.Get(identifier);
            var okResult = result as OkObjectResult;

            // Returns Success
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            Assert.AreEqual(jobResultModel, okResult.Value);
        }

        [Test]
        public void GetById_ShouldReturnErrorIfServiceThrowsException()
        {
            Guid identifier = Guid.NewGuid();
            string message = "Test message";
            _jobDataService.Setup(x => x.Exists(identifier, out message)).Returns(true);
            _jobDataService.Setup(x => x.Get(identifier)).Throws(new SystemException());

            Assert.Throws<SystemException>(() => _sortingJobController.Get(identifier));
        }

        #endregion v1.0/sorting-job/Get/Id

        #region v1.0/sorting-job/Get/Status

        [Test]
        public void GetByStatus_ShouldReturnBadRequestIfWrongStatusIsPassed()
        {
            string status = "WrongStatus";
            string message = "Test message";
            _jobDataService.Setup(x => x.ValidateStatus(status, out message)).Returns(false);

            var result = _sortingJobController.Get(status);
            var badRequestResult = result as BadRequestObjectResult;

            // Returns BadRequest
            Assert.IsNotNull(badRequestResult);
            Assert.AreEqual(400, badRequestResult.StatusCode);
            Assert.AreEqual(message, badRequestResult.Value);
        }

        [Test]
        public void GetByStatus_ShouldReturnSuccessIfRecordsExists()
        {
            string status = JobStatus.PENDING.ToString();
            var jobResultCollection = new List<JobResultModel>{ new JobResultModel
            {
                Identifier = Guid.NewGuid(),
                Input = new List<int> { 2, 1 },
                CreationDateTime = DateTime.UtcNow,
                JobStatus = JobStatus.PENDING.ToString()
            } };
            string message = string.Empty;
            _jobDataService.Setup(x => x.ValidateStatus(status, out message)).Returns(true);
            _jobDataService.Setup(x => x.Get(status)).Returns(jobResultCollection);

            var result = _sortingJobController.Get(status);
            var okResult = result as OkObjectResult;

            // Returns Success
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            CollectionAssert.AreEqual(jobResultCollection, okResult.Value as List<JobResultModel>);
        }

        [Test]
        public void GetByStatus_ShouldReturnErrorIfServiceThrowsException()
        {
            string status = "WrongStatus";
            string message = "Test message";
            _jobDataService.Setup(x => x.ValidateStatus(status, out message)).Returns(true);
            _jobDataService.Setup(x => x.Get(status)).Throws(new SystemException());

            Assert.Throws<SystemException>(() => _sortingJobController.Get(status));
        }

        #endregion v1.0/sorting-job/Get/Status

        #region v1.0/sorting-job/Post

        [Test]
        public void Post_ShouldReturnBadRequestIfArrayIsEmpty()
        {
            int[] inputArray = new int[0];
            string message = "Test Message";
            _jobDataService.Setup(x => x.IsValidPost(inputArray, out message)).Returns(false);

            var result = _sortingJobController.Post(inputArray);
            var badResult = result as BadRequestObjectResult;

            // Returns BadRequest
            Assert.IsNotNull(badResult);
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual(message, badResult.Value);
        }

        [Test]
        public void Post_ShouldReturnSuccessIfRecordsExists()
        {
            Guid identifier = Guid.NewGuid();
            int[] inputArray = new[] { 2, 1 };

            string message = string.Empty;
            _jobDataService.Setup(x => x.IsValidPost(inputArray, out message)).Returns(true);
            _jobDataService.Setup(x => x.Create(new List<int>() { 2, 1 })).Returns(identifier);

            var result = _sortingJobController.Post(inputArray);
            var okResult = result as OkObjectResult;

            // Returns Success
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            Assert.AreEqual(identifier, okResult.Value);
        }

        [Test]
        public void Post_ShouldReturnErrorIfServiceThrowsException()
        {
            int[] inputArray = new int[0];
            string message = "Test Message";

            _jobDataService.Setup(x => x.IsValidPost(inputArray, out message)).Returns(true);
            _jobDataService.Setup(x => x.Create(It.IsAny<List<int>>())).Throws(new SystemException());

            Assert.Throws<SystemException>(() => _sortingJobController.Post(inputArray));
        }

        #endregion v1.0/sorting-job/Post
    }
}
