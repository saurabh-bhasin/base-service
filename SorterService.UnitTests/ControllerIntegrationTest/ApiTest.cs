﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using SorterService.Controllers;
using SorterService.DataServices.Crud;
using SorterService.DataServices.Interfaces;
using SorterService.DataServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SorterService.UnitTests.Integration
{
    [TestFixture, Category("Controller")]
    public class ApiTests
    {
        private Mock<ILogger<SortingJobController>> _logger;
        private IJobDataService _jobDataService;
        private JobDataContext _jobDataContext;
        private SortingJobController _sortingJobController;

        [SetUp]
        public void SetUp()
        {
            _jobDataContext = new JobDataContext() { Jobs = new List<JobModel>() };
            _logger = new Mock<ILogger<SortingJobController>>();
            _jobDataService = new JobDataService(_jobDataContext);
            _sortingJobController = new SortingJobController(_logger.Object, _jobDataService);
        }

        [TearDown]
        public void Cleanup()
        {
            _jobDataContext.Jobs.Clear();
        }

        #region v1.0/sorting-job/Get/Id

        [Test]
        public void GetById_ShouldReturnNotFoundIfNoRecordExists()
        {
            Guid identifier = Guid.NewGuid();
            var result = _sortingJobController.Get(identifier);

            // Returns NotFound with message
            var notFoundObjectResult = result as NotFoundObjectResult;
            Assert.IsNotNull(notFoundObjectResult);
            Assert.AreEqual(404, notFoundObjectResult.StatusCode);
            Assert.AreEqual($"Job {identifier} does not exist", notFoundObjectResult.Value);
        }

        [Test]
        public void GetById_ShouldReturnSuccessIfRecordExists()
        {
            Guid identifier = Guid.NewGuid();
            var expectedJobResultModel = new JobResultModel
            {
                Identifier = identifier,
                Input = new List<int> { 2, 1 },
                CreationDateTime = DateTime.UtcNow,
                JobStatus = JobStatus.PENDING.ToString()
            };
            _jobDataContext.Jobs.Add(new JobModel
            {
                Identifier = identifier,
                CreationDateTime = expectedJobResultModel.CreationDateTime,
                JobStatus = JobStatus.PENDING,
                Input = expectedJobResultModel.Input
            });

            var result = _sortingJobController.Get(identifier);

            // Returns Success
            var okResult = result as OkObjectResult;
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            AssertJobModelEqual(expectedJobResultModel, okResult.Value as JobResultModel);

        }

        #endregion v1.0/sorting-job/Get/Id

        #region v1.0/sorting-job/Get/Status

        [Test]
        public void GetByStatus_ShouldReturnBadRequestIfWrongStatusIsPassed()
        {
            string status = "WrongStatus";

            var result = _sortingJobController.Get(status);
            var badRequestResult = result as BadRequestObjectResult;

            // Returns BadRequest with message
            Assert.IsNotNull(badRequestResult);
            Assert.AreEqual(400, badRequestResult.StatusCode);
            Assert.AreEqual($"{status} is not valid. Only valid status are Pending/Completed/Failed", badRequestResult.Value);
        }

        [Test]
        public void GetByStatus_ShouldReturnSuccessIfRecordsExists()
        {
            string status = JobStatus.PENDING.ToString();
            Guid identifier = Guid.NewGuid();
            var expectedResultCollection = new List<JobResultModel>{ new JobResultModel
            {
                Identifier = identifier,
                Input = new List<int> { 2, 1 },
                CreationDateTime = DateTime.UtcNow,
                JobStatus = JobStatus.PENDING.ToString()
            }};
            _jobDataContext.Jobs.Add(new JobModel
            {
                Identifier = identifier,
                CreationDateTime = expectedResultCollection[0].CreationDateTime,
                JobStatus = JobStatus.PENDING,
                Input = expectedResultCollection[0].Input
            });

            var result = _sortingJobController.Get(status);
            var okResult = result as OkObjectResult;

            // Returns Success
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            var jobResultCollection = okResult.Value as IEnumerable<JobResultModel>;
            expectedResultCollection.ForEach(expected =>
            {
                var actual = jobResultCollection.Single(y => expected.Identifier == y.Identifier);
                AssertJobModelEqual(expected, actual);
            });
        }

        #endregion v1.0/sorting-job/Get/Status

        #region v1.0/sorting-job/Post

        [Test]
        public void Post_ShouldReturnBadRequestIfArrayIsEmpty()
        {
            int[] inputArray = new int[0];

            var result = _sortingJobController.Post(inputArray);
            var badResult = result as BadRequestObjectResult;

            // Returns BadRequest
            Assert.IsNotNull(badResult);
            Assert.AreEqual(400, badResult.StatusCode);
            Assert.AreEqual($"Input cannot be empty array", badResult.Value);
        }

        [Test]
        public void Post_ShouldReturnSuccessIfRecordsExists()
        {
            int[] inputArray = new[] { 2, 1 };

            var result = _sortingJobController.Post(inputArray);
            var okResult = result as OkObjectResult;

            // Returns Success
            Assert.IsNotNull(okResult);
            Assert.AreEqual(200, okResult.StatusCode);
            Assert.IsNotNull(okResult.Value is Guid);
        }

        private void AssertJobModelEqual(JobResultModel expected, JobResultModel actual)
        {
            Assert.AreEqual(expected.Identifier, actual.Identifier);
            Assert.AreEqual(expected.CreationDateTime, actual.CreationDateTime);
            Assert.AreEqual(expected.DurationInSeconds, actual.DurationInSeconds);
            Assert.AreEqual(expected.JobStatus, actual.JobStatus);
            CollectionAssert.AreEqual(expected.Input, actual.Input);
            CollectionAssert.AreEqual(expected.Output, actual.Output);
        }

        #endregion v1.0/sorting-job/Post
    }
}
