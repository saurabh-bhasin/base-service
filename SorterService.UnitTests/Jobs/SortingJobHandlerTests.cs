using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using NUnit.Framework.Internal;
using SorterService.DataServices.Interfaces;
using SorterService.DataServices.Models;
using SorterService.Jobs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace SorterService.UnitTests.Jobs
{
    [TestFixture]
    public class SortingJobHandlerTests
    {
        private Mock<ILogger<SortingRequestHandler>> _logger;
        private Mock<IDataSortingService<int>> _dataSortingService;
        private JobDataContext _jobDataContext;
        private SortingRequestHandler _sortingJobHandler;

        [SetUp]
        public void SetUp()
        {
            _logger = new Mock<ILogger<SortingRequestHandler>>();
            _dataSortingService = new Mock<IDataSortingService<int>>();
            _jobDataContext = new JobDataContext();
            _sortingJobHandler = new SortingRequestHandler(_logger.Object, _dataSortingService.Object, _jobDataContext);
        }

        [Test]
        public void ShouldNotCallSortServiceIfNoPendingJobExists()
        {
            var task = _sortingJobHandler.Handle(new CancellationToken(false));

            // Should not call sort service
            _dataSortingService.Verify(x => x.SortData(It.IsAny<int[]>()), Times.Never);
        }

        [Test]
        public void VerifyJobOutputIfPendingJobExists()
        {
            Guid identifier = Guid.NewGuid();
            var data = new int[] { 1, 2 };
            var input = new JobModel
            {
                Identifier = identifier,
                Input = new List<int> { 2, 1 },
                CreationDateTime = DateTime.UtcNow,
                JobStatus = JobStatus.PENDING
            };

            _jobDataContext.Jobs.Add(input);
            _dataSortingService.Setup(x => x.SortData(new int[] { 2, 1 })).Returns(new int[] { 1, 2 });
            var task = _sortingJobHandler.Handle(new CancellationToken(false));

            // Should call sort service once
            _dataSortingService.Verify(x => x.SortData(It.IsAny<int[]>()), Times.Once);
            Assert.AreEqual(JobStatus.COMPLETED, input.JobStatus);
            Assert.AreEqual(new int[] { 1, 2 }, input.Output.ToArray());
            Assert.IsTrue(input.Duration > 0);
        }

        [Test]
        public void VerifyJobisSetToFailedIfPendingJobThrowsException()
        {
            var dataToSort = new int[] { 2, 1 };

            var input = new JobModel
            {
                Identifier = Guid.NewGuid(),
                Input = dataToSort.ToList(),
                CreationDateTime = DateTime.UtcNow,
                JobStatus = JobStatus.PENDING
            };
            var expectedOutput = new JobModel
            {
                Identifier = Guid.NewGuid(),
                Input = new List<int> { 1, 2 },
                CreationDateTime = DateTime.UtcNow,
                JobStatus = JobStatus.PENDING,
                Duration = 1,
                Output = new List<int> { 1, 2 }
            };

            _jobDataContext.Jobs.Add(input);
            _dataSortingService.Setup(x => x.SortData(dataToSort)).Throws<ArgumentException>();

            var task = _sortingJobHandler.Handle(new CancellationToken(false));
            Assert.AreEqual(JobStatus.FAILED, input.JobStatus);
            Assert.AreEqual(null, input.Output);
        }
    }
}
