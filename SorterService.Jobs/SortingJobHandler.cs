﻿using Microsoft.Extensions.Logging;
using SorterService.DataServices.Interfaces;
using SorterService.DataServices.Models;
using SorterService.Jobs.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SorterService.Jobs
{
    public class SortingRequestHandler : IJobHandler
    {
        private readonly ILogger _logger;
        private readonly IDataSortingService<int> _dataSortingService;
        private readonly JobDataContext _jobDataContext;

        public SortingRequestHandler(ILogger<SortingRequestHandler> logger, IDataSortingService<int> dataSortingService, JobDataContext jobDataContext)
        {
            _logger = logger;
            _dataSortingService = dataSortingService;
            _jobDataContext = jobDataContext;
        }

        public async Task Handle(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var job = _jobDataContext.Jobs.OrderBy(x => x.CreationDateTime).FirstOrDefault(x => x.JobStatus == JobStatus.PENDING);
                if (job != null)
                {
                    _logger.LogInformation($"Found 1 {JobStatus.PENDING} job for processing.");
                    try
                    {
                        job.Output = _dataSortingService.SortData(job.Input.ToArray()).ToList();
                        job.Duration = (DateTime.UtcNow - job.CreationDateTime).TotalSeconds;
                        job.JobStatus = JobStatus.COMPLETED;
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"Job Failed while processing {JobStatus.PENDING} jobs.", ex);
                        job.JobStatus = JobStatus.FAILED;
                    }
                }
                else
                {
                    // Log the info and exit
                    _logger.LogInformation($"No {JobStatus.PENDING} job found.");
                }

                await Task.Delay(5000, stoppingToken);
            }
        }
    }
}