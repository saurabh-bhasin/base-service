﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System.Net;

namespace SorterService.Filters.Exception
{
    public class UnknownExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var objResult = new ObjectResult(context.Exception.Message ?? "Unhandled error occured")
            {
                StatusCode = (int)HttpStatusCode.InternalServerError,
            };
            context.Result = objResult;
            context.ExceptionHandled = true;
        }
    }
}