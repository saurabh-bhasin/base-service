﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Linq;
using System;

namespace SorterService.Filters.Exception
{
    public class BadRequestResultFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var exceptions = new[] { typeof(FormatException).FullName, typeof(ArgumentException).FullName };

            if (exceptions.All(e => e != context.Exception.GetType().FullName)) { return; }

            context.Result = new BadRequestObjectResult(context.Exception.Message);
            context.ExceptionHandled = true;
        }
    }
}