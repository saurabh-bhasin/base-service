using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using SorterService.DataServices.Crud;
using SorterService.DataServices.Interfaces;
using SorterService.DataServices.Jobs;
using SorterService.DataServices.Models;
using SorterService.Filters.Constraints;
using SorterService.Filters.Exception;
using SorterService.Jobs;
using SorterService.Jobs.Interfaces;
using System.Collections.Generic;

namespace SorterService
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("api", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Sorting Service API",
                    Description = "Sorting Service API"
                });
            });
            services.AddMvc(options => options.Filters.Add(new BadRequestResultFilterAttribute()));
            services.AddScoped<IJobDataService, JobDataService>();
            services.AddSingleton(new JobDataContext());
            services.AddScoped<IDataSortingService<int>, NumberSortingService>();
            services.Configure<RouteOptions>(x => x.ConstraintMap.Add("jobstatusconstraint", typeof(JobStatusConstraint)));
            services.AddScoped<IJobHandler, SortingRequestHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            SetupPathBase(app, Configuration["PathBase"]);
            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

        private static void SetupPathBase(IApplicationBuilder app, string pathBase)
        {
            var trimmedPathBase = pathBase.TrimStart('/').TrimEnd('/');
            app.UseSwagger(c =>
            {
                c.PreSerializeFilters.Add((swaggerDoc, httpReq) =>
                {
                    c.SerializeAsV2 = true;
                    swaggerDoc.Servers = new List<OpenApiServer> { new OpenApiServer { Url = $"{httpReq.Scheme}://{httpReq.Host.Value}{pathBase}" } };
                });
                c.RouteTemplate = $"{trimmedPathBase}/documentation/{{documentName}}/swagger";
            });
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"/{trimmedPathBase}/documentation/api/swagger", "Order Manager Core API");
                c.RoutePrefix = $"{trimmedPathBase}/swagger";
            });
            app.UsePathBase(pathBase);
        }
    }
}
